function [output, j] = my_landweber_nonneg_freemtx(input, h, order, lambda, Nit)
m = length(input);
n = length(h);
xk = zeros(m-n+1, 1);
output = zeros(m-n+1, 1);
d = 1;
for i = 1:order
    d = conv(d,[1 -1]);
end
[H, ~] = freqz(h, 1);
[D, ~] = freqz(d,1);
alpha1 = 1/2*max(abs(H.^2));
alpha2 = 1/2*max(abs(D.^2));
j = zeros(1, Nit);
for i = 1:Nit
    xk_old = xk;
    xk = (convt(h, input)+(alpha1*xk-convt(h, conv(h, xk)))+lambda*(alpha2*xk-convt(d, conv(d, xk))))/(alpha1+lambda*alpha2);
    j(i) = sum(sqrt((xk-xk_old).^2));
end
for i = 1:m-n+1
    if xk(i) < 0
        output(i) = 0;
    else
        output(i) = xk(i);
    end
end
end