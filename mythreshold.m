function y = mythreshold(x,sigma)
y = zeros(1, length(x));
o1 = 10/(2*sigma - 10)^2;
o2 = -10/(2*sigma - 10)^2;
for i = 1:length(x)
    if abs(x(i)) <= 2 * sigma
        y(i) = 0;
    elseif x(i) <= 0
        y(i) = o1 * (x(i) + 10)^2 - 10;
    else
        y(i) = o2 * (x(i) - 10)^2 + 10;
    end
end
end