function upimg = upsamplefilter(img, filter)
bilinear = [1/4 1/2 1/4; 1/2 1 1/2; 1/4 1/2 1/4];
bicubic = [-1/8 0 5/8 1 5/8 0 -1/8]'*[-1/8 0 5/8 1 5/8 0 -1/8];
[m, n] = size(img);
upimg = zeros(m*2, n*2);
for i = 1:m*2
    for j = 1:n*2
        if ~mod(i, 2) || ~mod(j,2)
            upimg(i, j) = 0;
        else
            upimg(i, j) = img((i+1)/2, (j+1)/2);
        end
    end
end
if strcmp(filter, 'Bilinear')
    upimg = myconv2(upimg, bilinear);
elseif strcmp(filter, 'Bicubic')
    upimg = myconv2(upimg, bicubic);
else
    msg = 'wrong input';
    error(msg);
end
x = max(upimg);
x = max(x);
y = min(upimg);
y = min(y);
for i = 1:m*2
    for j = 1:n*2
        upimg(i,j) = round((upimg(i,j)-y)*255/(x-y));
    end
end
upimg = uint8(upimg);
end