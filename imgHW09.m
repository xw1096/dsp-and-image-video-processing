img = imread('/home/code/Documents/Samples/lena_gray.bmp');
i = double(img);
[m, n] = size(img);
blocks = m*n/64;
x1 = my_qdct(img, 0.5);
x2 = my_qdct(img, 1);
x3 = my_qdct(img, 2);
x4 = my_qdct(img, 4);
x5 = my_qdct(img, 8);
y1 = my_qdctinv(x1, 0.5);
y2 = my_qdctinv(x2, 1);
y3 = my_qdctinv(x3, 2);
y4 = my_qdctinv(x4, 4);
y5 = my_qdctinv(x5, 8);
x = [0.5 1 2 4 8];
peaksnr = [psnr(y1, i) psnr(y2, i) psnr(y3, i) psnr(y4, i) psnr(y5, i)];
non = [sum(sum(x1~=0)) sum(sum(x2~=0)) sum(sum(x3~=0)) sum(sum(x4~=0)) sum(sum(x5~=0))]/blocks;
subplot(3, 2, 1)
imshow(img)
title('original image')
subplot(3, 2, 2)
imagesc(y1)
axis image
colormap(gray)
title('scaling factor: 0.5')
subplot(3, 2, 3)
imagesc(y2)
axis image
colormap(gray)
title('scaling factor: 1')
subplot(3, 2, 4)
imagesc(y3)
axis image
colormap(gray)
title('scaling factor: 2')
subplot(3, 2, 5)
imagesc(y4)
axis image
colormap(gray)
title('scaling factor: 4')
subplot(3, 2, 6)
imagesc(y5)
axis image
colormap(gray)
title('scaling factor: 8')
figure
plot(x, peaksnr);
title('PSNR vs. quantization scaling')
xlabel('quantization scaling factors')
ylabel('PSNR')
figure
plot(x, non);
title('non-zero coefficients/blocks vs. quantization scaling')
xlabel('quantization scaling factors')
ylabel('non-zero coefficients/blocks')
figure
plot(non, peaksnr);
title('PSNR vs. non-zero coefficients/blocks')
xlabel('non-zero coefficients/blocks')
ylabel('PSNR')