function x = halfband(d)
if ~mod(d, 2)
    msg = 'please input odd number';
    error(msg)
end
conv_matrix = [1 1];
for i = 1:d
    conv_matrix = conv(conv_matrix, [1 1]);
end
l = length(conv_matrix);
linear_matrix1 = zeros(d, l + d -1);
for i = 1:d
    linear_matrix1(i,:) = [zeros(1,d-i),conv_matrix,zeros(1,i -1)];
end
linear_matrix2 = linear_matrix1(:, 2:2:end-1);
n = [zeros(1,(d-1)/2) 1 zeros(1,(d-1)/2)];
x1 = linear_matrix2'\n';
x = conv(conv_matrix,x1);
end