signal1 = load('/home/code/Documents/MATLAB/bumps.txt');
signal2 = load('/home/code/Documents/MATLAB/pwsmooth.txt');
%% B
h0 = (1 + sqrt(3)) / (4 * sqrt(2));
h1 = (3 + sqrt(3)) / (4 * sqrt(2));
h2 = (3 - sqrt(3)) / (4 * sqrt(2));
h3 = (1 - sqrt(3)) / (4 * sqrt(2));
w = [h0 h1 h2 h3; h3 -h2 h1 -h0];
inv_signal = mywaveletinv(wavelet, w);
figure
subplot(2, 1, 1)
plot(signal1)
xlim([0 1023])
title('original signal')
subplot(2, 1, 2)
plot(inv_signal)
xlim([0 1023])
title('inverse signal')
%% C
for i = 4:2:8
    filter = mydaubechies(i);
    figure
    subplot(3, 2, 1)
    stem(filter(1, :))
    title('AVE impulse response')
    [h, w] = freqz(filter(1, :), 1);
    subplot(3, 2, 3)
    plot(w/pi, abs(h))
    title('AVE frequency response')
    subplot(3, 2, 5)
    zplane(filter(1, :))
    title('AVE zero diagram')
    subplot(3, 2, 2)
    stem(filter(2, :))
    title('DIFF impulse response')
    [h, w] = freqz(filter(2, :), 1);
    subplot(3, 2, 4)
    plot(w/pi, abs(h))
    title('DIFF frequency response')
    subplot(3, 2, 6)
    zplane(filter(2, :))
    title('DIFF zero diagram')
end
%% D
for i = 4:2:8
    filter = mydaubechies(i);
    wavelet1 = mywavelet(signal1, filter, 5);
    wavelet2 = mywavelet(signal2, filter, 5);
    figure
    subplot(7, 2, 1)
    plot(signal1)
    title('original bumps')
    subplot(7, 2, 3)
    stem(wavelet1(6).data)
    title('bumps c5')
    for j = 2:6
    subplot(7, 2, 2*j+1)
    stem(wavelet1(7-j).data)
    string = ['bumps d',num2str(7-j)];
    title(string)
    end
    subplot(7, 2, 2)
    plot(signal2)
    title('original pwsmooth')
    subplot(7, 2, 4)
    stem(wavelet2(6).data)
    title('pwsmooth c5')
    for j = 3:7
    subplot(7, 2, 2*j)
    stem(wavelet1(8-j).data)
    string = ['pwsmooth d',num2str(8-j)];
    title(string)
    end   
end
%% E
for i = 4:2:8
    filter = mydaubechies(i);
    hd = filter(2, :);
    h_tot = 1;
    figure
    for j = 1:10
        subplot(5, 2, j)
        for k = 1:2^j-1
            h_tot = conv(h_tot, hd);
        end
        subplot(5, 2, j)
        stem(h_tot)
        string = ['level = ',num2str(j)];
        title(string)
    end
end