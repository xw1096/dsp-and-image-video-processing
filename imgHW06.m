obj = VideoReader('/home/code/Documents/MATLAB/test3.mp4');
mov(1:10) = struct('cdata', zeros(obj.Height, obj.Width, 1, 'uint8'), 'colormap', []);
for i = 1:10
    temp = readFrame(obj, 90+i);
    temp = rgb2gray(temp);
    mov(i).cdata = temp;
end
%% integer-pel EBMA
[predicted, mvx, mvy] = my_EBMA(mov(4).cdata, mov(1).cdata, 8, 32, 'integer');
figure
subplot(2,2,1)
imshow(mov(1).cdata)
hold on
[x, y] = meshgrid(1:8:168, 1:8:136);
quiver(x, y, mvx, mvy)
axis equal
title('estimated motion field')
subplot(2,2,2)
imshow(mov(4).cdata)
title('anchor frame')
subplot(2,2,3)
imshow(predicted)
peaksnr = psnr(predicted, mov(4).cdata);
a = num2str(peaksnr);
title(['predicted frame ' a 'dB'])
subplot(2,2,4)
error = predicted - mov(4).cdata;
imagesc(error)
%% half-pel EBMA
tic
[predicted, mvx, mvy] = my_EBMA(mov(7).cdata, mov(4).cdata, 4, 16, 'half');
toc
figure
subplot(2,2,1)
imshow(mov(4).cdata)
%hold on
%[x, y] = meshgrid(1:8:168, 1:8:136);
%quiver(x, y, mvx, mvy)
%axis equal
title('estimated motion field')
subplot(2,2,2)
imshow(mov(7).cdata)
title('anchor frame')
subplot(2,2,3)
imshow(predicted)
peaksnr = psnr(predicted, mov(7).cdata);
a = num2str(peaksnr);
title(['predicted frame ' a 'dB'])
subplot(2,2,4)
error = predicted - mov(7).cdata;
imagesc(error)
%% HBMA
tic
[fp, mvx, mvy] = my_HBMA(mov(7).cdata, mov(4).cdata, 3, 32, 1);
toc
[i, j] = size(fp);
figure
subplot(2,2,1)
imshow(mov(4).cdata)
%hold on
%[m, n] = size(mvx);
%quiver(x, y, mvx, mvy)
%axis equal
title('estimated motion field')
subplot(2,2,2)
imshow(mov(7).cdata)
title('anchor frame')
subplot(2,2,3)
imshow(fp)
peaksnr = psnr(fp, mov(4).cdata(1:i, 1:j));
a = num2str(peaksnr);
title(['predicted frame ' a 'dB'])
subplot(2,2,4)
error = fp - mov(7).cdata(1:i, 1:j);
imagesc(error)