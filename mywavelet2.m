function x = mywavelet2(img, filter, level)
% please use the filter constructed from the myDaubechies function
img = double(img);
x(1:3*level+1) = struct('data', [], 'type', []);
filter_2d = cell(1, 4);
filter_2d{1} = filter(2, :)'*filter(2, :);
filter_2d{2} = filter(2, :)'*filter(1, :);
filter_2d{3} = filter(1, :)'*filter(2, :);
filter_2d{4} = filter(1, :)'*filter(1, :);
name = cell(1, 4);
name{1} = 'hh';
name{2} = 'hl';
name{3} = 'lh';
name{4} = 'll';
c_temp = img;
for i = 1:level
    for j = 1:3
        x((i-1)*3+j).data = downsample(downsample(conv2(filter_2d{j}, c_temp), 2)', 2)';
        x((i-1)*3+j).type = [name{j}, num2str(i)];
    end
    c_temp = downsample(downsample(conv2(filter_2d{4}, c_temp), 2)', 2)';
end
x(3*level+1).data = c_temp;
x(3*level+1).type = [name{4}, num2str(level)];
end