%% 1.
obj = VideoReader('/home/code/Documents/MATLAB/test.mp4');
mov = find_change(obj, 30, 20);
output = VideoWriter('/home/code/Documents/MATLAB/test.avi');
output.FrameRate = 30;
open(output)
for i = 1:30
    temp = mov(i).cdata;
    writeVideo(output, temp);
end
close(output)
%% 2.
obj = VideoReader('/home/code/Documents/MATLAB/test2.mp4');
temp1 = read(obj, 30);
temp2 = read(obj, 39);
temp1 = rgb2gray(temp1);
temp2 = rgb2gray(temp2);
template = temp1(16:119, 270:384);
[x, y, matchblock] = my_EBMA(template, temp2);
temp1(16, 270:384) = 255;
temp1(119, 270:384) = 255;
temp1(16:119, 270) = 255;
temp1(16:119, 384) = 255;
temp2(x(1), y(1):y(2)) = 255;
temp2(x(2), y(1):y(2)) = 255;
temp2(x(1):x(2), y(1)) = 255;
temp2(x(1):x(2), y(2)) = 255;
subplot(1,2,1)
imshow(temp1)
subplot(1,2,2)
imshow(temp2)