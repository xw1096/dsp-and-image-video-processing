x = load('/home/code/Documents/MATLAB/bumps.txt');
n = randn(1, 1024)';
n = 0.251 * n;
xn = x + n;
%% daubechise noise reduction
[c1, d1] = daubechies(xn');
[c2, d2] = daubechies(c1);
[c3, d3] = daubechies(c2);
[c4, d4] = daubechies(c3);
d12 = nonthreshold(d1, 0.251);
d22 = nonthreshold(d2, 0.251);
d32 = nonthreshold(d3, 0.251);
d42 = nonthreshold(d4, 0.251);
c32 = invdaubechies(c4, d42);
c22 = invdaubechies(c32, d32);
c12 = invdaubechies(c22, d22);
x2 = invdaubechies(c12, d12);
figure;
subplot(3,1,1)
plot(x)
subplot(3,1,2)
plot(xn)
subplot(3,1,3)
plot(x2)
%% haar noise reduction
[c1, d1] = haar(xn');
[c2, d2] = haar(c1);
[c3, d3] = haar(c2);
[c4, d4] = haar(c3);
d12 = nonthreshold(d1, 0.251);
d22 = nonthreshold(d2, 0.251);
d32 = nonthreshold(d3, 0.251);
d42 = nonthreshold(d4, 0.251);
c32 = invhaar(c4, d42);
c22 = invhaar(c32, d32);
c12 = invhaar(c22, d22);
x2 = invhaar(c12, d12);
figure;
subplot(3,1,1)
plot(x)
subplot(3,1,2)
plot(xn)
subplot(3,1,3)
plot(x2)
%% my threshold
[c1, d1] = daubechies(xn');
[c2, d2] = daubechies(c1);
[c3, d3] = daubechies(c2);
[c4, d4] = daubechies(c3);
d12 = mythreshold(d1, 0.251);
d22 = mythreshold(d2, 0.251);
d32 = mythreshold(d3, 0.251);
d42 = mythreshold(d4, 0.251);
c32 = invdaubechies(c4, d42);
c22 = invdaubechies(c32, d32);
c12 = invdaubechies(c22, d22);
x2 = invdaubechies(c12, d12);
figure;
subplot(3,1,1)
plot(x)
subplot(3,1,2)
plot(xn)
subplot(3,1,3)
plot(x2)