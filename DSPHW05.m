%% 50% overlap STFT and inverse STFT
[signal, fs] = audioread('/home/code/Documents/MATLAB/sp1.wav');
signal_cut = signal(1: 18944);
windows = ones(1, 512);
[B, f, t]  = my_stft(signal_cut', 5120, fs, windows, 256);
figure
imagesc(t, f, abs(B));
cmap = flipud(gray);
colormap(cmap)
axis xy
xlabel('Time (sec.)') 
ylabel('Frequency (Hz)') 
title('Spectrogram') 
colorbar
inv_B = my_istft(B, windows, 256);
figure
subplot(2,1,1)
plot(signal_cut)
title('original signal')
subplot(2,1,2)
plot(inv_B)
title('inverse signal')
%% another perfect reconstruction windows of 50% overlap STFT and inverse STFT
n = 0:127;
m = 128:-1:1;
windows = [sqrt(1/128*n) sqrt(1/128*m)];
[B, ~, ~]  = my_stft(signal_cut', 2560, fs, windows, 128);
inv_B = my_pr(B, windows, 128);
figure
subplot(3,1,1)
stem(windows)
title('window')
subplot(3,1,2)
plot(signal_cut)
title('original siganl')
subplot(3,1,3)
plot(inv_B)
title('perfect reconstruction')
%% perfect reconstruction condition for a STFT with 2/3 overlapping
windows = 1/sqrt(3)*ones(1, 128*3);
[B, ~, ~]  = my_stft(signal_cut', 3840, fs, windows, 256);
inv_B = my_pr(B, windows, 256);
figure
subplot(3,1,1)
stem(windows)
title('window')
subplot(3,1,2)
plot(signal_cut)
title('original siganl')
subplot(3,1,3)
plot(inv_B)
title('perfect reconstruction')
%% use the STFT and thresholding to reduce the noise level in a noisy speech signal
[noise_speech, fs] = audioread('/home/code/Documents/MATLAB/noisy_speech.wav');
windows = ones(1, 512);
[B, f, t]  = my_stft(noise_speech', 5120, fs, windows, 256);
figure
subplot(1,2,1)
imagesc(t, f, 20*log10(abs(B)))
colormap(cmap);
caxis([-20 30])
colorbar
axis xy
xlabel('Time (sec.)') 
ylabel('Frequency (Hz)') 
title('Spectrogram of noisy speech signal') 
sigma = 0.25;
[~, n] = size(B);
for i = 1:n
    B(:, i) = nonthreshold(B(:, i), sigma);
end
subplot(1,2,2)
imagesc(t, f, 20*log10(abs(B)))
colormap(cmap);
caxis([-20 30])
colorbar
axis xy
xlabel('Time (sec.)') 
ylabel('Frequency (Hz)') 
title('Spectrogram after thresholding') 
inv_B = my_istft(B, windows, 256);
figure
subplot(2,1,1)
plot(noise_speech);
title('noise speech')
subplot(2,1,2)
plot(inv_B)
title('processed signal')
%% Parseval Energy Identity
[signal, fs] = audioread('/home/code/Documents/MATLAB/sp1.wav');
signal_cut = signal(1: 18944);
time_power = sum(signal_cut.^2);
freq_power = 0;
windows = ones(1, 512);
[B, ~, ~]  = my_stft(signal_cut', 5120, fs, windows, 256);
[m, n] = size(B);
for k = 1:n
    for l = 1: m
        freq_power = freq_power + B(l,k)*conj(B(l,k));
    end
end
freq_power = freq_power/5120/2;