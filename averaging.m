function z = averaging(f, size)
filter = (1/size^2) * ones(size, size);
z = myconv2(f, filter);
end