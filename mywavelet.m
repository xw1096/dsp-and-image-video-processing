function x = mywavelet(signal, filter, level)
% please use the filter constructed from the myDaubechies function
x(1:level+1) = struct('data', [], 'type', []);
for i = 1:level - 1
    c = upfirdn(signal, filter(1, :), 1, 2);
    d = upfirdn(signal, filter(2, :), 1, 2);
    string = ['d', num2str(i)];
    x(i).data = d;
    x(i).type = string;
    signal = c;
end
c = upfirdn(signal, filter(1, :), 1, 2);
d = upfirdn(signal, filter(2, :), 1, 2);
string = ['d', num2str(level)];
x(level).data = d;
x(level).type = string;
x(level+1).data = c;
x(level+1).type = 'c';
end