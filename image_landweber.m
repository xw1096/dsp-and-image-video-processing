function [output, j] = image_landweber(input, h, order, lambda, Nit)
[m, n] = size(input);
[x, y] = size(h);
xk = zeros(m-x+1, n-y+1);
d = 1;
for i = 1:order
    d = conv(d, [1 -1]);
end
d = d'*d;
H = freqz2(h);
D = freqz2(d);
alpha1 = 1/2*max(max(abs(H.^2)));
alpha2 = 1/2*max(max(abs(D.^2)));
j = zeros(1, Nit);
for i = 1:Nit
    xk_old = xk;
    xk = (convt2(h, input)+(alpha1*xk-convt2(h, conv2(h, xk)))+lambda*(alpha2*xk-convt2(d, conv2(d, xk))))/(alpha1+lambda*alpha2);
    j(i) = sum(sum(sqrt((xk-xk_old).^2)));
end
output = xk;
end