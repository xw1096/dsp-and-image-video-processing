obj = VideoReader('/home/code/Documents/MATLAB/MOV_0057.3gp');
mov(1:10) = struct('cdata', zeros(obj.Height, obj.Width, 1, 'uint8'), 'colormap', []);
for i = 1:20
    temp = read(obj, i);
    temp = rgb2gray(temp);
    mov(i).cdata = temp;
end
implay(mov)