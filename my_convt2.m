function output = my_convt2(img, hc, hr)
% the 2d filter should be separable to column vector and row vector
img = double(img);
k = length(hc);
l = length(hr);
[m, n] = size(img);
output = zeros(m-k+1, n-l+1);
temp = zeros(m, n-l+1);
for i = 1:m
    temp(i, :) = convt(hr, img(i, :));
end
for j = 1:n-l+1
    output(:, j) = convt(hc, temp(:, j));
end
end