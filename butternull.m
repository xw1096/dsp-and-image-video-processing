function [b, a] = butternull(order, null_frequency)
k = 50;
null = pi - null_frequency;
b = poly([-ones(1,order) exp(1j*null_frequency) exp(-1j*null_frequency)]);
b2 = poly([-ones(1,2*order) exp(1j*null_frequency) exp(1j*null_frequency) exp(-1j*null_frequency) exp(-1j*null_frequency)]);
s2 = poly([ones(1,2*order) exp(1j*null) exp(1j*null) exp(-1j*null) exp(-1j*null)]);
c2 = b2 - k * s2;
r2 = roots(c2);
r = r2(abs(r2) < 0.9);
a = poly(r);
b = b*sum(a)/sum(b);
fvtool(b,a)
end