function [edgeimg,threshold] = edgedetect(img, percent)
%% set parameters and sobel filter
[m, n] = size(img);
hx = 1/4*[-1 -2 -1; 0 0 0; 1 2 1];
hy = 1/4*[-1 0 1; -2 0 2; -1 0 1];
%% find the horizontal and vertical gradients then combine together
imgx = myconv2(img, hx);
imgy = myconv2(img, hy);
imgxy = imgx.^2 + imgy.^2;
%% renormalize imgxy
x = max(imgxy);
x = max(x);
y = min(imgxy);
y = min(y);
for i = 1:m
    for j = 1:n
        imgxy(i,j) = round((imgxy(i,j)-y)*255/(x-y));
    end
end
%% find threshold (only 5% pixels should be edges)
hist = fasthist(imgxy);
t_decide = 0;
for i = 256:-1:1
    t_decide = t_decide + hist(i);
    if t_decide/(m*n) >= percent
        threshold = i;
        break
    end
end
%% edge detect and show image
edgeimg = zeros(m,n);
for i = 1:m
    for j = 1:n
       if imgxy(i,j) + 1 < threshold
           edgeimg(i,j) = 0;
       else
           edgeimg(i,j) = 255;
       end
    end
end
end