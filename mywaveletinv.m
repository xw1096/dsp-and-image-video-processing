function x = mywaveletinv(struct, filter)
% please use the filter constructed from the myDaubechies function
level = length(struct) - 1;
m = length(filter);
c_temp = struct(level + 1).data;
for i = level:-1:2
    c_temp = upfirdn(struct(i).data, filter(2, end:-1:1), 2, 1) + upfirdn(c_temp, filter(1, end:-1:1), 2, 1);
    n = length(struct(i-1).data);
    c_temp = c_temp(m:m+n-1);
end
x = upfirdn(struct(1).data, filter(2, end:-1:1), 2, 1) + upfirdn(c_temp, filter(1, end:-1:1), 2, 1);
x = x(m:end-m+1);
end