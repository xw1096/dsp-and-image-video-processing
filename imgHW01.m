%% main
figure(1)
[Matrix, map] = imread('/home/code/Downloads/DSC_6660.jpg');
subplot(1,3,1);
imshow(Matrix);
grayscale = zeros(500,350);
rgb2ymatrix = [0.257 0.504 0.098];
for i = 1:500
    for j = 1:350
        singlepoint = double([Matrix(i,j,1) Matrix(i,j,2) Matrix(i,j,3)]);
        grayscale(i,j) = rgb2ymatrix*singlepoint'+16;
    end
end
grayscale = int8(grayscale);
subplot(1,3,2);
imshow(grayscale);
negativescale = -grayscale;
subplot(1,3,3);
imshow(negativescale);

% test the speed of function
%% method 1
tic
h = zeros(256,1);
for L = 0:255
    for i = 1:500
        for j = 1:350
            if grayscale(i,j) == L
                h(L+1) = h(L+1)+1;
            end
        end
    end9, Email:  yw523 at nyu dot edu. Homepage: http://eeweb.poly.edu/~yao

Course Schedule: Monday 10:30-1:00 AM, MTC 9.011


end
figure(2)
bar(h);
toc
%% method 2
tic
h = zeros(256,1);
for i = 1:500
    for j = 1:350;
        f = grayscale(i,j);
        h(f+1) = h(f+1)+1;
    end
end
figure(3)
bar(h);
toc
%% method 3
tic
h = zeros(256,1);
for L = 0:255
    h(L+1) = sum(sum(grayscale == L));
end
figure(4)
bar(h);
toc
%}
fasthist(grayscale);
hist_eq(grayscale);