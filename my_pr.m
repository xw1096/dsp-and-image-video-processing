function B = my_pr(x, windows, overlap)
[m, n] = size(x);
R = length(windows);
B = zeros(1, n *(R - overlap)+R);
temp1 = zeros(1, n *(R - overlap)+R);
windows_padding = [windows zeros(1,length(x(:,1))-length(windows))];
for i = 0:n-1
    temp = ifft(x(:,i+1)').*windows_padding;
    temp = temp(1:R);
    temp1(i*(R-overlap)+1:i*(R-overlap)+R) = temp;
    %temp(i*(R-overlap)+1:i*(R-overlap)+1+R) = temp(i*(R-overlap)+1:i*(R-overlap)+1+R).*windows;
    B = B + temp1;
    temp1 = zeros(1, n *(R - overlap)+R);
end
end