function output = my_findmissing(input)
m = length(input);
j = 1;
for i = 1:m
    if isnan(input(i))
        %sc(j, :) = [zeros(1, i-1) 1 zeros(1, m-i)];
        j = j+1;
        input(i) = 0;
    end
end
sc = zeros(j, m);
j = 1;
for i = 1:m
    if input(i) == 0
        sc(j, :) = [zeros(1, i-1) 1 zeros(1, m-i)];
        j = j+1;
    end
end
d = [1 -2 1];
D = zeros(m -2, m);
for i = 1:m-2
    D(i, :) = [zeros(1, i-1) d zeros(1, m-i-2)];
end
v = -(sc*(D'*D)*input)\(sc*(D'*D)*sc');
output = input+sc'*v';
end