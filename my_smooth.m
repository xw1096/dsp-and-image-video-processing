function output = my_smooth(input, weight, order)
d = 1;
for i = 1:order
    d = conv(d,[1 -1]);
end
m = length(input);
n = length(d);
D = zeros(m -n, m);
for i = 1:m-n
    D(i, :) = [zeros(1, i-1) d zeros(1, m-i-n+1)];
end
I = eye(m);
output = input\(I+weight*(D'*D));
end