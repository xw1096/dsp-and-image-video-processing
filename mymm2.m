%% initial system and parameters
sender_state = 0;
receiver1_state = 0;
receiver2_state = 0;
pkt_in_serve1 = 0;
pkt_in_serve2 = 0;
lambda = 225.0;
rate = 1000000.0;
mean_pktsize = 512.0;
unit_time = 0.0001;
total_time = 65.0;
record = zeros(total_time/0.1+1, 4);
now_time = 0.0;
queue = zeros(1, 200000);
arpkt = 0.0;
depkt = 0.0;
pkt_in_serve = 0.0;
record(1, :) = [now_time, length(find(queue(:))), arpkt, depkt];
send_gap = exprnd(1/lambda);
%% start system
for i = 1.0: total_time/unit_time
    now_time = now_time + 0.0001;
    %% record statistics every 0.1s
    if rem(i, 1000.0) == 0
            record(i/1000+1, :) = [now_time, length(find(queue(:))), arpkt, depkt];
    end
    %% send packets
    if sender_state == 0
        send_time = now_time + send_gap;
        sender_state = 1;
    end
    if now_time >= send_time
        pkt_size = exprnd(mean_pktsize);
        queue(1,length(find(queue(:)))+1) = pkt_size;
        arpkt = arpkt + 1;
        send_gap = exprnd(1/lambda);
        sender_state = 0;
    end
    %% server process
    if receiver1_state == 0 && receiver2_state == 0 && isempty(find(queue(:), 1))
        continue
    elseif receiver1_state == 1
        pkt_in_serve1 = pkt_in_serve1 - 6.25;
        if receiver2_state == 1
            pkt_in_serve2 = pkt_in_serve2 - 6.25;
        elseif ~isempty(find(queue(:), 1))
            receiver2_state = 1;
            pkt_in_serve2 = queue(1,1); 
            queue = [queue(2:200000) 0];
            pkt_in_serve2 = pkt_in_serve2 - 6.25;
        end
    elseif receiver2_state == 1
        pkt_in_serve2 = pkt_in_serve2 - 6.25;
        if ~isempty(find(queue(:), 1))
            receiver1_state = 1;
            pkt_in_serve1 = queue(1,1);
            queue = [queue(2:200000) 0];
            pkt_in_serve1 = pkt_in_serve1 - 6.25;
        end
    else
        receiver1_state = 1;
        pkt_in_serve1 = queue(1,1);
        queue = [queue(2:200000) 0];
        pkt_in_serve1 = pkt_in_serve1 - 6.25;
        if ~isempty(find(queue(:), 1))
            receiver2_state = 1;
            pkt_in_serve2 = queue(1,1);
            queue = [queue(2:200000) 0];
            pkt_in_serve2 = pkt_in_serve2 - 6.25;
        end
    end
    if pkt_in_serve1 <= 0 && receiver1_state == 1
        receiver1_state = 0;
        depkt = depkt + 1;         
    end
    if pkt_in_serve2 <= 0 && receiver2_state == 1
        receiver2_state = 0;
        depkt = depkt + 1;
    end
end