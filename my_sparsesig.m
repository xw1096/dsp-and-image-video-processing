function xorg = my_sparsesig(n, k, x)
xorg=zeros(n,1); %initialization of the sparse vector as a zero vector 'xorg'
idx=randperm(n);  %we are calculating the index of first 'k' non zero elements through this step
xorg(idx(1:k))=x*rand(k,1); %take first 'k' elements of 'idx' & fill those index with a random value
end