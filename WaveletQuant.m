function [NonZeroNum, PSNR, outimg] = WaveletQuant(inimg, order, QS)
inimg = double(inimg);
filter = mydaubechies(order);
x = mywavelet2(inimg, filter, 3);
% quantization
for i = 1:6
    x(i).data = floor((x(i).data+QS/2)/QS)*QS;
end
for i = 7:10
    x(i).data = floor((x(i).data+QS/2-128)/QS)*QS+128;
end
NonZeroNum = 0;
for i = 1:10
    NonZeroNum = nnz(x(i).data)+NonZeroNum;
end
outimg = mywaveletinv2(x, filter);
PSNR = psnr(outimg, inimg);
end