function [c, d] = daubechies(x)
h0 = (1 + sqrt(3)) / (4 * sqrt(2));
h1 = (3 + sqrt(3)) / (4 * sqrt(2));
h2 = (3 - sqrt(3)) / (4 * sqrt(2));
h3 = (1 - sqrt(3)) / (4 * sqrt(2));
if mod(length(x), 2) ~= 0
    x = [x, zeros(1,3)];
    c = h0 * x(1:2:length(x) - 3) + h1 * x(2:2:length(x)-2) + h2 * x(3:2:length(x)-1) + h3 * x(4:2:length(x));
    d = h3 * x(1:2:length(x) - 3) - h2 * x(2:2:length(x)-2) + h1 * x(3:2:length(x)-1) - h0 * x(4:2:length(x));
else
    x = [x, zeros(1,2)];
    c = h0 * x(1:2:length(x) - 3) + h1 * x(2:2:length(x)-2) + h2 * x(3:2:length(x)-1) + h3 * x(4:2:length(x));
    d = h3 * x(1:2:length(x) - 3) - h2 * x(2:2:length(x)-2) + h1 * x(3:2:length(x)-1) - h0 * x(4:2:length(x));
end
end