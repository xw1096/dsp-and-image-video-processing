function f = convt2(h, g)
[mh, nh] = size(h);
[mg, ng] = size(g);
f = conv2(h(mh:-1:1,nh:-1:1), g);
f = f(mh:mg, nh:ng);
end