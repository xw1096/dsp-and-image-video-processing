function imgmedian = mymedfilt2(img, order)
%% set parameters
[m, n] = size(img);
bound = (order - 1) / 2;
middle = order - bound;
imgmedian = zeros(m, n);
set1 = zeros(order,order);
%% apply filter and show image
for i = 1:m
    for j = 1:n
        if i <= bound || i > m-bound || j <= bound || j > n-bound % simplified boundary treatment
            imgmedian(i,j) = img(i, j);
        else
            for k = -bound:bound
                for l = -bound:bound
                    set1(middle+k,middle+l) = img(i-k,j-l);
                end
            end
            set2 = reshape(set1,1,order^2);
            imgmedian(i,j) = median(set2);
        end
    end
end
imgmedian = uint8(imgmedian);
end