function mov = find_change(obj, num, threshold)
mov_temp(1:num) = struct('cdata', zeros(obj.Height, obj.Width, 1, 'uint8'), 'colormap', []);
for i = 1:num
    temp = read(obj,i);
    temp = rgb2gray(temp);
    mov_temp(i).cdata = temp;
end
for i = 1:num-1
    temp1 = mov_temp(i).cdata;
    temp2 = mov_temp(i+1).cdata;
    for m = 1:obj.Height
        for n = 1:obj.Width
            change = temp2(m, n) - temp1(m, n);
            if abs(change) >= threshold
                temp2(m, n) = 255;
            else
                temp2(m, n) = 0;
            end
        end
    end
    mov(i+1).cdata = temp2;
end
end