function x = invdaubechies(c, d)
h0 = (1 + sqrt(3)) / (4 * sqrt(2));
h1 = (3 + sqrt(3)) / (4 * sqrt(2));
h2 = (3 - sqrt(3)) / (4 * sqrt(2));
h3 = (1 - sqrt(3)) / (4 * sqrt(2));
x = zeros(1, length(c) * 2);
x(1) = h0 * c(1) + h3 * d(1);
x(2) = h1 * c(1) - h2 * d(1);
x(3:2:length(x)-1) = h0 * c(2:length(c)) + h2 * c(1:length(c)-1) + h3 * d(2:length(d)) + h1 * d(1:length(d)-1);
x(4:2:length(x)) = h1 * c(2:length(c)) + h3 * c(1:length(c)-1) - h2 * d(2:length(d)) - h0 * d(1:length(d)-1);
end