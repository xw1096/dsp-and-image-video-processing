%% Biorthogonal wavelet filters
% struct filter
x = halfband(7);
r = roots(x);
r1 = r(abs(r) < 0.9);
r2 = r(abs(r) > 1.1);
g0 = poly([-1*ones(1, 4) r1(imag(r1)~=0)' r2(imag(r2)~=0)']);
h0 = poly([-1*ones(1, 4) r1(imag(r1)==0)' r2(imag(r2)==0)']);
h0 = h0/sum(h0)*sqrt(2);
g0 = g0/sum(g0)*sqrt(2);
h1 = zeros(1, length(g0));
g1 = zeros(1, length(h0));
for n = 0:8
    h1(n+1) = (-1)^n*g0(n+1);
end
for n = 0:6
    g1(n+1) = (-1)^(n+1)*h0(n+1);
end
subplot(2, 2, 1)
stem(h0)
title('IMPULSE RESPONSE h_0(n)')
subplot(2, 2, 2)
stem(g0)
title('IMPULSE RESPONSE g_0(n)')
subplot(2, 2, 3)
stem(h1)
title('IMPULSE RESPONSE h_1(n)')
subplot(2, 2, 4)
stem(g1)
title('IMPULSE RESPONSE g_1(n)')
% verify perfect reconstruction
figure
stem(1/2*conv(g0,h0)+1/2*conv(g1,h1))
%% Real DFT
x = dftmtx(8);
y = zeros(size(x));
y(:, 8) = x(:, 5);
for n = 2:4
    y(:, (n-1)*2) = real(x(:, n));
    y(:, (n-1)*2+1) = imag(x(:, n));
end
figure
for n = 1:8
    subplot(8, 1, n)
    stem(y(:, n))
end
orient tall
%%  Principle component analysis (PCA)
img = imread('IMG_2616.jpg');
img = rgb2gray(img);
img = double(img);
[m, n] = size(img);
c_length = m*n/16;
x = zeros(16, c_length);
for i = 0:m/4-1                                    % turn to vector
    for j = 0:n/4-1
        tmp(1:4) = img(i*4+1,j*4+1:(j+1)*4);
        tmp(5:8) = img(i*4+2,j*4+1:(j+1)*4);
        tmp(9:12) = img(i*4+3,j*4+1:(j+1)*4);
        tmp(13:16) = img(i*4+4,j*4+1:(j+1)*4);
        x(:,i*400+j+1) = tmp';    
    end
end
cx = x*x';
[E, D] = eig(cx);
d = diag(D);
[~, k] = sort(-d);
E = E(:, k);
P = E';
temp = zeros(4, 4);
figure
for i = 1:16
    for j = 0:3
        temp(j+1, :) = P(i, j*4+1:j*4+4);
    end
    subplot(4,4,i)
    imagesc(temp)
    colormap(gray)
    axis image off
    caxis([-0.6 0.6])
    colorbar
end
orient tall
y = P*x;
y(5:16, :) = 0;                                   % set 12 of 16 coefficients to zero
x_new = P'*y;
img_new = zeros(m, n);
for i = 0:m/4-1                                    % reconstruction
    for j = 0:n/4-1
        img_new(i*4+1,j*4+1:(j+1)*4) = x_new(1:4, i*400+j+1); 
        img_new(i*4+2,j*4+1:(j+1)*4) = x_new(5:8, i*400+j+1);
        img_new(i*4+3,j*4+1:(j+1)*4) = x_new(9:12, i*400+j+1);
        img_new(i*4+4,j*4+1:(j+1)*4) = x_new(13:16, i*400+j+1);  
    end
end
figure 
subplot(2, 1, 1)
imagesc(img)
colormap(gray)
axis image off
title('original image')
subplot(2, 1, 2)
imagesc(img_new)
colormap(gray)
axis image off
title('12 of 16 coeffiecients to zero')
orient tall