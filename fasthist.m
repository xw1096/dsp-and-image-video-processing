function h = fasthist(matrix)
matrix = uint8(matrix);
[M, N] = size(matrix);
h = zeros(256,1);
for i = 1:M
    for j = 1:N;
        f = matrix(i,j);
        h(f+1) = h(f+1)+1;
    end
end