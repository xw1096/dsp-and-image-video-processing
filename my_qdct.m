function x = my_qdct(img, scale)
img = double(img);
Q = [16 11 10 16 24 40 51 61
     12 12 14 19 26 58 60 55
     14 13 16 24 40 57 69 56 
     14 17 22 29 51 87 80 62
     18 22 37 56 68 109 103 77
     24 35 55 64 81 104 113 92 
     49 64 78 87 103 121 120 101 
     72 92 95 98 112 100 103 99] * scale;
[m, n] = size(img);
x = zeros(8*ceil(m/8), 8*ceil(n/8));
for i = 1:floor(m/8)
    for j = 1:floor(n/8)
        temp = dct2(img((i-1)*8+1:i*8, (j-1)*8+1:j*8), [8 8]);
        temp = floor((temp+Q/2)./Q);
        x((i-1)*8+1:i*8, (j-1)*8+1:j*8) = temp;
    end
end
end