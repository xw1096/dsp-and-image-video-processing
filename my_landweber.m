function [output, j] = my_landweber(input, h, order, lambda, Nit)
m = length(input);
n = length(h);
xk = zeros(m-n+1, 1);
H = zeros(m, m-n+1);
for i = 1:m-n+1
    H(:, i) = [zeros(1, i-1) h zeros(1, m-n+1-i)]';
end
d = 1;
for i = 1:order
    d = conv(d,[1 -1]);
end
l = length(d);
D = zeros(m-n+2-l, m-n+1);
for i = 1:m-n+2-l
    D(i, :) = [zeros(1, i-1) d zeros(1, m-i-n-l+2)];
end
alpha1 = 1/2*max(eig(H'*H));
alpha2 = 1/2*max(eig(D'*D));
I = eye(m-n+1);
j = zeros(1, Nit);
for i = 1:Nit
    xk_old = xk;
    xk = (H'*input+(alpha1*I-(H'*H))*xk+lambda*(alpha2*I-(D'*D))*xk)/(alpha1+lambda*alpha2);
    j(i) = sum(sqrt((xk-xk_old).^2));
end
output = xk;
end