function y = nonthreshold(x, sigma)
y = zeros(1, length(x));
for i = 1: length(x)
    if abs(x(i)) <= 2 * sigma
        y(i) = 0;
    else
        y(i) = x(i);
    end
end
end