function mov = my_stab(input)
l = length(input);
[m, n] = size(input(1).cdata);
mov(1:l) = struct('cdata', zeros(m, n, 1, 'uint8'), 'colormap', []);
ref = double(input(1).cdata);
mov(1).cdata = input(1).cdata;
for i = 2:l
%     {temp = double(input(i).cdata);
%     Q = zeros(3, 3);
%     r = zeros(3, 1);
%     for j = 1:m
%         for k = 1:n
%             Q = Q + [1 k j; k k*k k*j; j k*j j*j];
%         end
%     end
    temp = my_HBMA(ref, input(i).cdata, 3, 16, 2);
    mov(i).cdata = temp;
end
end