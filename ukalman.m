%% setting the initial parameters 
N = 100;                                         % measure N times parameters
n = 3;                                          % dimesion of parameters
q = 0.1;                                        % process noise covariance
r = 0.1;                                          % measurement noise covariance
Q = q^2*eye(n);
R = r^2*eye(n);
f=@(x)[x(2);x(3);0.05*x(1)*(x(2)+x(3))];    % non-linear state transition function
h = @(x)[x(1); x(2); x(3)];                     % transformation matrix
s = [0; 0; 1];                                  % initail state
x = s + q*randn(3, 1);
P = eye(n);
prediction = zeros(n, N);
measurement = zeros(n, N);
real = zeros(n, N);
%%
for k = 1:N
    z = h(s) + r*randn(3, 1);
    real(:, k) = s;
    measurement(:, k) = z;
    %[x, P] = nukf(f, x, P, h, z, Q, R);
    [x, P] = ukf(f, h, x, P, Q, R, z);
    prediction(:, k) = x;
    s = f(s) + q*randn(3, 1);
end 
figure
for k = 1:3
    name = ['estimation state ', num2str(k)];
    subplot(3, 1, k)
    p1=plot(real(k, :), 'g');
    hold on
    p2=plot(measurement(k, :), 'k+');
    p3=plot(prediction(k, :), 'r--');
    ylabel(name)
    xlabel('time')
    legend([p1, p2, p3], 'real state', 'measurement state', 'estimated state')
end