function z = myconv2(f, h) % f for image, h for filter
[m, n] = size(f);
f = double(f);
bound = (length(h) - 1) / 2; % decide innner boundary
middle = length(h) - bound;
z = zeros(m,n);
stack = 0;
for i = 1:m
    for j = 1:n
        if i <= bound || i > m-bound || j <= bound || j > n-bound % simplified boundary treatment
            z(i,j) = 0;
        else
            for k = -bound:bound
                for l = -bound:bound
                    stack = stack + f(i-k,j-l) * h(middle+k,middle+l);
                end
            end
            z(i,j) = stack;
            stack = 0;
        end
    end
end
end