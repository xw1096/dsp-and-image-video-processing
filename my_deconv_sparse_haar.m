function output = my_deconv_sparse_haar(input, h, level, lambda, Nit)
xk = 0*convt(h, input);
haar = mydaubechies(2);
xk = mywavelet(xk, haar, level);
%j = zeros(1, Nit);
%hinv = conv(h, haar(1,:));
[H, ~] = freqz(h, 1);
alpha = max(abs(H.^2));
t = lambda/(2*alpha);
for k = 1:Nit
    x = conv(h, mywaveletinv(xk, haar));
    g = mywavelet(convt(h, input-x), haar, level);
    
    for j = 1:level+1
        
        xj = xk(j).data;
        xj = xj + g(j).data/alpha;
        m = length(xj);
        if j < level+1
            for i = 1:m
                if abs(xj(i))-t < 0
                    xj(i) = 0;
                else
                    xj(i) = sign(xj(i))*(abs(xj(i))-t);
                end
            end
        end
        xk(j).data = xj;
    end
end
output = mywaveletinv(xk, haar);
end