function [B, f, t] = my_stft(signal, zero_padding, fs, windows, overlap)
R = length(windows);
N = length(signal);
padding = zeros(1, zero_padding - R);
i = 1;
j = 1;
while N - i >= R
  i = i + R - overlap;
  j = j + 1;
end
B = zeros(zero_padding, j);
for m = 0:j-1
    if N - m * (R - overlap)-1 >= R
        fft_vector = [windows.*signal(m * (R - overlap)+1:m * (R - overlap)+R) padding];
    else
        fft_vector = [windows(1:length(signal(m * (R - overlap)+1:end))).*signal(m * (R - overlap)+1:end) zeros(1, zero_padding - length(signal(m * (R - overlap)+1:end)))];
    end
    B(:,m+1) = fft(fft_vector)';
end
t = [0 N/fs];
f = [0 fs];
end