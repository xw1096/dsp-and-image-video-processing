function [c, d] = haar(x)
if mod(length(x), 2) ~= 0
    x = [x, zeros(1,1)];
end
c = (1 / sqrt(2)) * x(1:2:length(x) - 1) + (1 / sqrt(2)) * x(2:2:length(x));
d = (1 / sqrt(2)) * x(1:2:length(x) - 1) - (1 / sqrt(2)) * x(2:2:length(x));
end