function downimg = mydownsample2(img, filter)
averaging = [1/2 1 1/2]'*[1/2 1 1/2];
[m, n] = size(img);
downimg = zeros(m/2, n/2);
for i = 1:m/2
    for j = 1:n/2
        downimg(i, j) = img((i-1)*2+1, (j-1)*2+1);
    end
end
if strcmp(filter, 'Averaging')
    downimg = myconv2(downimg, averaging);
end
x = max(downimg);
x = max(x);
y = min(downimg);
y = min(y);
for i = 1:m/2
    for j = 1:n/2
        downimg(i,j) = round((downimg(i,j)-y)*255/(x-y));
    end
end
downimg = uint8(downimg);
end