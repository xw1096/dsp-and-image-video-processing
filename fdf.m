N = 11;
M = (N-1)/2;
Ak = [1 1 1 1 1 1 1 1 1 1 1]; % samples of A(w)
k = 0:N-1;
d=9.3;
W = exp(1j*2*9.3*pi/N);
h = ifft(Ak.*W.^(-M*k));
h=h';
stem(h);