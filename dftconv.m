function y=dftconv(x,h)
X=fft(x);
H=fft(h);
Y=H.*X;
y=ifft(Y);