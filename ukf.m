function [x, p] = ukf(statefun, measurefun, x, p, Q, R, z)
L = numel(x);
K = numel(z);
%% set parameters
alpha = 1e-3;
ki = max(0, 3-L);
beta = 2;
lambda = alpha^2*(L+ki)-L;
wm = [lambda/(L+lambda) 1/(2*(L+lambda))*ones(1,2*L)];
wc = [lambda/(L+lambda)+(1-alpha^2+beta) 1/(2*(L+lambda))*ones(1,2*L)];
%% sigma vector
X = [x, x(:,ones(1, L))+sqrt(L+lambda)*chol(p)', x(:,ones(1, L))-sqrt(L+lambda)*chol(p)'];
%% unscented transform
XP = zeros(size(X));
x_ = zeros(L, 1);
for k = 1:2*L+1
    XP(:, k) = statefun(X(:, k));
    x_ = x_+wm(k)*XP(:, k);
end
p_ = (XP-x_(:, ones(1, 2*L+1)))*diag(wc)*(XP-x_(:, ones(1, 2*L+1)))'+Q;
XM = zeros(K, 2*L+1);
z_ = zeros(K, 1);
X_ = [x_, x_(:,ones(1, L))+sqrt(L+lambda)*chol(p)', x_(:,ones(1, L))-sqrt(L+lambda)*chol(p)'];
for k = 1:2*L+1
    XM(:, k) = measurefun(X_(:, k));
    z_ = z_+wm(k)*XM(:, k);
end
pm = (XM-z_(:, ones(1, 2*L+1)))*diag(wc)*(XM-z_(:, ones(1, 2*L+1)))'+R;
pm_ = (XP-x_(:, ones(1, 2*L+1)))*diag(wc)*(XM-z_(:, ones(1, 2*L+1)))';
kalman_gain = pm_/pm;
x = x_+kalman_gain*(z-z_);
p = p_-kalman_gain*pm*kalman_gain';
%p = nearestSPD(p);
end
