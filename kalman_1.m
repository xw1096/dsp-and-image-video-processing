z = (1:100);
noise = 4*randn(1, 100);
z = z + noise;

x = [0; 0];
P = [1 0; 0 1];
F = [1 1; 0 1];
Q = [0.0001 0; 0 0.0001];
H = [1 0];
R = 1;

figure
subplot(2, 1, 1)
xlim([0 100])
hold on
subplot(2, 1, 2)
xlim([0 100])
hold on
for i = 1:100
    x_ = F*x;
    P_ = F*P*F' + Q;
    K = P_*H'/(H*P_*H'+R);
    x = x_+K*(z(i)-H*x_);
    P = (eye(2)-K*H)*P_;
    subplot(2, 1, 1)
    p1=plot(i, x(1), 'b*');
    p2=plot(i, z(i), 'k+');
    subplot(2, 1, 2)
    p3=plot(x(1), x(2), 'b*');
end
subplot(2, 1, 1)
p4=plot([0 100], [0 100], 'r--');
xlabel('Obeservation time')
ylabel('Position')
legend([p1, p2, p4],'estimate position','measured position','real position')
subplot(2, 1, 2)
p5=plot([0 100], [1 1], 'r--');
xlabel('Obeservation time')
ylabel('Speed')
legend([p3,p5],'estimate speed','real speed')