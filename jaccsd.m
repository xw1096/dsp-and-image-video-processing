function [z, T] = jaccsd(fun, x)
% use complex step differentiation to derive jacobian matrix
% z as fun output
% T as jacobian matrix
%
z = fun(x);
n = numel(x);
m = numel(z);
T = zeros(m, n);
h = n*eps;
for k = 1:n
    x1 = x;
    x1(k) = x1(k) + h*1i;
    T(:, k) = imag(fun(x1))/h;
end