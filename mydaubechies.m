function filter = mydaubechies(order)
filter = zeros(2, order);
p = halfband(order - 1);
r2 = roots(p);
r = r2(abs(r2) < 0.9);
a = poly([-1*ones(1, order/2) r']);
a = a/sum(a)*sqrt(2);
filter(1, :) = a;
N = length(a);
for n = 0:N-1
    filter(2, n+1) = (-1)^n*a(end-n);
end
end