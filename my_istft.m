function B = my_istft(x, windows, overlap)
[m, n] = size(x);
R = length(windows);
B = zeros(1, n *(R - overlap)+R);
for i = 0:n - 2
    temp = ifft(x(:,i+1)');
    temp = temp(1:R)./windows;
    B(i*(R-overlap)+1: i*(R-overlap)+R-overlap) = temp(1:R-overlap);
end
temp = ifft(x(:,n));
temp = temp(1:windows)'./windows;
B((n-1)*(R-overlap)+1:(n-1)*(R-overlap)+R) = temp;
end