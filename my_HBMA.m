function [fp, mvx_new, mvy_new] = my_HBMA(anchor_frame, target_frame, level, range, block)
anchor_frame = double(anchor_frame);
target_frame = double(target_frame);
for o = level-1:-1:0
    f1 = imresize(anchor_frame, (1/2)^o);
    f2 = imresize(target_frame, (1/2)^o);
    if o == level-1
        [~, mvx_new, mvy_new] = my_EBMA(f1, f2, block, range*(1/2)^o, 'integer');
        %[m, n] = size(mvx);
    else
        mvx = 2*mvx_new;
        mvy = 2*mvy_new;
        [m_new, n_new] = size(mvx_new);
        mvx_new = zeros(2*m_new+1, 2*n_new+1);
        mvy_new = zeros(2*m_new+1, 2*n_new+1);
        %[m_new, n_new] = size(mvx)
        [m, n] = size(f1);
        for i = 1:block*2:m-block*2
            for j = 1:block*2:n-block*2
                for k = 0:1
                    for l = 0:1
                        MAD_min = 255*block*block;
                        iblk = floor((i-1)/(2*block)+1);
                        jblk = floor((j-1)/(2*block)+1);
                        for q = -1:1
                            for w = -1:1
                                if q+i+mvy(iblk, jblk)+k*block < 1
                                    numy1 = 1;
                                    numy2 = 1+block-1;
                                elseif q+i+k*block+block-1+mvy(iblk, jblk) > m
                                    numy1 = q+i+mvy(iblk, jblk)+k*block-1;
                                    numy2 = q+i+k*block+block-1+mvy(iblk, jblk)-1;
                                else
                                    numy1 = q+i+mvy(iblk, jblk)+k*block;
                                    numy2 = q+i+k*block+block-1+mvy(iblk, jblk);
                                end
                                if w+j+mvx(iblk, jblk)+l*block < 1
                                    numx1 = 1;
                                    numx2 = 1+block-1;
                                elseif w+j+l*block+block-1+mvx(iblk, jblk) > n
                                    numx1 = w+j+mvx(iblk, jblk)+l*block-1;
                                    numx2 = w+j+l*block+block-1+mvx(iblk, jblk)-1;
                                else
                                    numx1 = w+j+mvx(iblk, jblk)+l*block;
                                    numx2 = w+j+l*block+block-1+mvx(iblk, jblk);
                                end
                                MAD = sum(sum(abs(f1(i+k*block:i+block-1+k*block, j+l*block:j+block-1+l*block)-f2(numy1:numy2, numx1:numx2))));
                                if MAD<MAD_min
                                    MAD_min = MAD;
                                    dy = numy1-i-k*block;
                                    dx = numx1-j-l*block;
                                end
                            end
                        end
                        iblk = floor((i+k*block-1)/block+1);
                        jblk = floor((j+l*block-1)/block+1);
                        mvx_new(iblk, jblk) = dx;
                        mvy_new(iblk, jblk) = dy;
                        if o == 0
                        fp(i+k*block:i+(k+1)*block-1, j+l*block:j+(l+1)*block-1) = f2(i+dy+k*block:i+dy+(k+1)*block-1, j+dx+l*block:j+dx+(l+1)*block-1);
                        end
                    end
                end
            end
        end
    end
end
[m, n] = size(fp);
for i = 1:m
    for j = 1:n
        if fp(i, j) == 0
            fp(i, j) = target_frame(i, j);
        end
    end
end
fp = uint8(fp);
end