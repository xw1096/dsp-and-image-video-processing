function [predicted_frame, mvx, mvy] = my_EBMA(anchor_frame, target_frame, block, range, type)
anchor_frame = double(anchor_frame);
target_frame = double(target_frame);
[m, n] = size(anchor_frame);
predicted_frame = zeros(m, n);
switch type
    case {'integer'}
        for i = 1:block:m-block
            for j = 1:block:n-block
                MAD_min = 256*block*block;
                if i<range+1 && j<range+1
                    for k = -i+1:1:range
                        for l = -j+1:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end
                elseif i<range+1 && j>n-range-block+1
                     for k = -i+1:1:range
                        for l = -range:1:n-block-j+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                     end           
                elseif i>m-range-block+1 && j>n-range-block+1
                    for k = -range:1:m-block-i+1
                        for l = -range:1:n-block-j+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end                      
                elseif i>m-range-block+1 && j<range+1
                    for k = -range:1:m-block-i+1
                        for l = -j+1:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end       
                elseif i<range+1
                    for k = -i+1:1:range
                        for l = -range:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif i>m-range-block+1
                    for k = -range:1:m-block-i+1
                        for l = -range:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif j<range+1
                    for k = -range:1:range
                        for l = -j+1:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif j>n-range-block+1
                    for k = -range:1:range
                        for l = -range:1:n-j-block+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                else
                    for k = -range:1:range
                        for l = -range:1:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-target_frame(i+k:i+k+block-1, j+l:j+l+block-1))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                end
                predicted_frame(i:i+block-1, j:j+block-1) = target_frame(i+dy:i+dy+block-1, j+dx:j+dx+block-1);
                iblk = floor((i-1)/block+1);
                jblk = floor((j-1)/block+1);
                mvx(iblk, jblk) = dx;
                mvy(iblk, jblk) = dy;
            end
        end
    case {'half'}
        target_frame = uint8(target_frame);
        f3 = imresize(target_frame, 2, 'bilinear');
        f3 = double(f3);
        for i = 1:block:m-block
            for j = 1:block:n-block
                MAD_min = 256*block*block;
                if i<range+1 && j<range+1
                    for k = -i+1:0.5:range
                        for l = -j+1:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end
                elseif i<range+1 && j>n-range-block+1
                     for k = -i+1:0.5:range
                        for l = -range:0.5:n-block-j+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                     end           
                elseif i>m-range-block+1 && j>n-range-block+1
                    for k = -range:0.5:m-block-i+1
                        for l = -range:0.5:n-block-j+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end                      
                elseif i>m-range-block+1 && j<range+1
                    for k = -range:0.5:m-block-i+1
                        for l = -j+1:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end
                        end
                    end       
                elseif i<range+1
                    for k = -i+1:0.5:range
                        for l = -range:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif i>m-range-block+1
                    for k = -range:0.5:m-block-i+1
                        for l = -range:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif j<range+1
                    for k = -range:0.5:range
                        for l = -j+1:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                elseif j>n-range-block+1
                    for k = -range:0.5:range
                        for l = -range:0.5:n-j-block+1
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                else
                    for k = -range:0.5:range
                        for l = -range:0.5:range
                            MAD = sum(sum(abs(anchor_frame(i:i+block-1, j:j+block-1)-f3(2*(i+k):2:2*(i+k+block-1), 2*(j+l):2:2*(j+l+block-1)))));
                            if MAD<MAD_min
                                MAD_min = MAD;
                                dy = k;
                                dx = l;
                            end                    
                        end
                    end
                end
                predicted_frame(i:i+block-1, j:j+block-1) = f3(2*(i+dy):2:2*(i+dy+block-1), 2*(j+dx):2:2*(j+dx+block-1));
                iblk = floor((i-1)/block+1);
                jblk = floor((j-1)/block+1);
                mvx(iblk, jblk) = dx;
                mvy(iblk, jblk) = dy;
            end
        end
end
for i = 1:m
    for j = 1:n
        if predicted_frame(i, j) == 0
            predicted_frame(i, j) = target_frame(i, j);
        end
    end
end
predicted_frame = uint8(predicted_frame);
end