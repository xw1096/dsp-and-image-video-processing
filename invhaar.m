function x = invhaar(c, d)
x = zeros(1,length(c)*2);
x(1:2:length(x)-1) = 1/sqrt(2)*c + 1/sqrt(2)*d;
x(2:2:length(x)) = 1/sqrt(2)*c - 1/sqrt(2)*d;
end