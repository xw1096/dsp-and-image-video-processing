% DISCRETE SQUARE ERROR
% filter length
N = 31;
M = (N-1)/2;
L = 2^8;
w = [0:L]*pi/L; % 0 <= omega <= pi
DA = (1./sinc(w/3)).*(w<=0.45*pi);
D = [DA, DA(L:-1:2)]; % periodize
w0 = 0.4*pi;
w1 = 0.5*pi;
WA = 10*(w<=w0)+(w>=w1);
W = [WA, WA(L:-1:2)]; % periodize
% construct q(k)
q = ifft(W);
% construct Q1, Q2, Q
Q1 = toeplitz(q([0:M]+1));
Q2 = hankel(q([0:M]+1),q([M:2*M]+1));
Q = (Q1 + Q2)/2;
% construct b
b = ifft(W.*D);
b = b([0:M]+1)'; % just need n=0:M coeffs
% solve linear system to get a(n)
a = Q\b;
% form impulse response h(n)
h = [a(M+1:-1:2); 2*a(1); a(2:M+1)]/2;
plot(abs(fft([h' zeros(1,300)])));