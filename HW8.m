N=61;
M=(N-1)/2;
w1=0.28*pi;
w2=0.32*pi;
w3=0.58*pi;
w4=0.62*pi;
f1=w1/pi;
f2=w2/pi;
f3=w3/pi;
f4=w4/pi;
q=[f1+3*(f3-f2)+10*(1-f4),f1*sinc(f1*[1:2*M])+3*f3*sinc(f3*[1:2*M])-3*f2*sinc(f2*[1:2*M])-10*f4*sinc(f4*[1:2*M])];
Q1=toeplitz(q([0:M]+1));
Q2=hankel(q([0:M]+1),q([M:2*M]+1));
Q=(Q1+Q2)/2;
b=3*f3*sinc(f3*[0:M]')-3*f2*sinc(f2*[0:M]');
a=Q\b;
h=[a(M+1:-1:2); 2*a(1); a(2:M+1)]/2;
L=6100;
plot(abs(fft([h' zeros(1,L-N)])));
H=fft([h' zeros(1,L-N)]);
k=0:L-1;
W=exp(1i*2*pi/L);
A=H.*W.^(M*k);
A=real(A);
