%% edge detection
img = imread('/home/code/Documents/Samples/Lena.tiff');
img = rgb2gray(img);
[imgedge1, t1] = edgedetect(img, 0.05);
[imgedge2, t2] = edgedetect(img, 0.1);
figure
subplot(1,2,1)
imshow(imgedge1)
title(['edge inclue 5% pixels, threshold = ',num2str(t1)])
subplot(1,2,2)
imshow(imgedge2)
title(['edge inclue 10% pixels, threshold = ',num2str(t2)])
%% median filter
imgnoise1 = imnoise(img, 'salt & pepper', 0.05);
imgnoise2 = imnoise(img, 'salt & pepper', 0.2);
imgmedian1_3 = mymedfilt2(imgnoise1, 3);
imgmedian1_5 = mymedfilt2(imgnoise1, 5);
imgmedian1_7 = mymedfilt2(imgnoise1, 7);
imgmedian2_3 = mymedfilt2(imgnoise2, 3);
imgmedian2_5 = mymedfilt2(imgnoise2, 5);
imgmedian2_7 = mymedfilt2(imgnoise2, 7);
figure
subplot(1,3,1)
imshow(imgmedian1_3)
title('3 x 3')
subplot(1,3,2)
imshow(imgmedian1_5)
title('5 x 5')
subplot(1,3,3)
imshow(imgmedian1_7)
title('7 x 7')
figure
subplot(1,3,1)
imshow(imgmedian2_3)
title('3 x 3')
subplot(1,3,2)
imshow(imgmedian2_5)
title('5 x 5')
subplot(1,3,3)
imshow(imgmedian2_7)
title('7 x 7')
%% 3 x 3 square structing element BW
img = imread('/home/code/Documents/Samples/barbara_gray.bmp');
img = im2bw(img);
se1 = strel('square', 3);
imgdilate_3 = imdilate(img, se1);
imgerode_3 = imerode(img, se1);
figure;
title('BW image')
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgdilate_3)
title('dilation with 3x3')
subplot(1,3,3)
imshow(imgerode_3)
title('erosion with 3x3')
imgopen_3 = imopen(img, se1);
imgclose_3 = imclose(img, se1);
figure;
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgopen_3)
title('open with 3x3')
subplot(1,3,3)
imshow(imgclose_3)
title('close with 3x3')
%% 7 x 7 square structuring element gray scale
se2 = strel('square', 7);
imgdilate_7 = imdilate(img, se2);
imgerode_7 = imerode(img, se2);
figure;
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgdilate_7)
title('dilation with 7x7')
subplot(1,3,3)
imshow(imgerode_7)
title('erosion with 7x7')
imgopen_7 = imopen(img, se2);
imgclose_7 = imclose(img, se2);
figure;
title('BW image')
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgopen_7)
title('open with 7x7')
subplot(1,3,3)
imshow(imgclose_7)
title('close with 7x7')
%% 3 x 3 square structing element gray scale
img = imread('/home/code/Documents/Samples/barbara_gray.bmp');
imgdilate_3 = imdilate(img, se1);
imgerode_3 = imerode(img, se1);
figure;
title('gray scale')
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgdilate_3)
title('dilation with 3x3')
subplot(1,3,3)
imshow(imgerode_3)
title('erosion with 3x3')
imgopen_3 = imopen(img, se1);
imgclose_3 = imclose(img, se1);
figure;
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgopen_3)
title('open with 3x3')
subplot(1,3,3)
imshow(imgclose_3)
title('close with 3x3')
%% 7 x 7 square structuring element gray scale
imgdilate_7 = imdilate(img, se2);
imgerode_7 = imerode(img, se2);
figure;
title('gray scale')
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgdilate_7)
title('dilation with 7x7')
subplot(1,3,3)
imshow(imgerode_7)
title('erosion with 7x7')
imgopen_7 = imopen(img, se2);
imgclose_7 = imclose(img, se2);
figure;
subplot(1,3,1)
imshow(img)
title('original image')
subplot(1,3,2)
imshow(imgopen_7)
title('open with 7x7')
subplot(1,3,3)
imshow(imgclose_7)
title('close with 7x7')
