function x = mywaveletinv2(struct, filter)
filter_2d = cell(1, 4);
filter_2d{1} = filter(2, :)'*filter(2, :);
filter_2d{2} = filter(2, :)'*filter(1, :);
filter_2d{3} = filter(1, :)'*filter(2, :);
filter_2d{4} = filter(1, :)'*filter(1, :);
level = (length(struct)-1)/3;
[mh, nh] = size(filter_2d{1});
c_temp = struct(3*level+1).data;
for i = level:-1:2
    hh = conv2(filter_2d{1}(end:-1:1, end:-1:1), upsample(upsample(struct((i-1)*3+1).data, 2)', 2)');
    hl = conv2(filter_2d{2}(end:-1:1, end:-1:1), upsample(upsample(struct((i-1)*3+2).data, 2)', 2)');
    lh = conv2(filter_2d{3}(end:-1:1, end:-1:1), upsample(upsample(struct((i-1)*3+3).data, 2)', 2)');
    ll = conv2(filter_2d{4}(end:-1:1, end:-1:1), upsample(upsample(c_temp, 2)', 2)');
    c_temp = hh + hl + lh + ll;
    [mg, ng] = size(struct((i-2)*3+1).data);
    c_temp = c_temp(mh:mh+mg-1, nh:nh+ng-1);
end
hh = conv2(filter_2d{1}(end:-1:1, end:-1:1), upsample(upsample(struct(1).data, 2)', 2)');
hl = conv2(filter_2d{2}(end:-1:1, end:-1:1), upsample(upsample(struct(2).data, 2)', 2)');
lh = conv2(filter_2d{3}(end:-1:1, end:-1:1), upsample(upsample(struct(3).data, 2)', 2)');
ll = conv2(filter_2d{4}(end:-1:1, end:-1:1), upsample(upsample(c_temp, 2)', 2)');
x = hh + hl + lh + ll;
x = x(mh:end-mh, nh:end-nh);
end