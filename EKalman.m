%% setting the initial parameters 
N = 50;                                         % measure N times parameters
n = 3;                                          % dimesion of parameters
q = 0.1;                                        % process noise covariance
r = 2;                                          % measurement noise covariance
Q = q^2*eye(n);
R = r^2*eye(n);
f = @(x)[x(1)+0.5; x(2)+0.1; cos(x(2)*x(1))];     % non-linear state transition function
h = @(x)[x(1); x(2); x(3)];                     % transformation matrix
s = [0; 1; 1];                                  % initail state
x = s + q*randn(3, 1);
P = eye(n);
prediction = zeros(n, N);
measurement = zeros(n, N);
real = zeros(n, N);
%% Extended kalman filter iteration
for k = 1:N
    z = h(s) + r*randn(3, 1);                         % get measurement data
    real(:, k) = s;
    measurement(:, k) = z;
    [x1, F] = jaccsd(f, x);
    P = F*P*F' + Q;
    [z1, H] = jaccsd(h, x1);
    K = P*H'/(H*P*H'+R);
    x = x1 + K*(z - z1);
    P = P-K*H*P;
    prediction(:, k) = x;
    s = f(s) + q*randn(3, 1);
end
%%  plot result
figure
for k = 1:3
    name = ['estimation state ', num2str(k)];
    subplot(3, 1, k)
    p1=plot(real(k, :), 'g');
    hold on
    p2=plot(measurement(k, :), 'k+');
    p3=plot(prediction(k, :), 'r--');
    ylabel(name)
    xlabel('time')
    legend([p1, p2, p3], 'real state', 'measurement state', 'estimated state')
end