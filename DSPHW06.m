%% 1)
signal = load('/home/code/Documents/MATLAB/ECG_noisy_incomplete.txt');
signal_complete = my_findmissing(signal);
signal_smooth = my_smooth(signal_complete, 10, 2);
figure
subplot(3, 1, 1)
plot(signal)
title('original signal')
subplot(3, 1, 2)
plot(signal_complete)
title('Missing sample estimation')
subplot(3, 1, 3)
plot(signal_smooth)
title('smoothing')
%% 2)
N = 300;
n = (0:N-1)';       
w = 5;
n1 = 70;
n2 = 130;
x = 2.1 * exp(-0.5*((n-n1)/w).^2) - 0.5*exp(-0.5*((n-n2)/w).^2).*(n2 - n);
h = n .* (0.9 .^n) .* sin(0.2*pi*n);
y = conv(h, x);
yn = y + 0.2 * randn(length(y), 1);
[deconv_sig, j] = my_landweber(yn, h', 1, 2, 400);
figure
subplot(5, 1, 1)
plot(x)
title('input')
subplot(5, 1, 2)
plot(h)
title('impose response')
subplot(5, 1, 3)
plot(yn)
title('noise output')
subplot(5, 1, 4)
plot(deconv_sig)
title('deconvolution')
subplot(5, 1, 5)
plot(j)
title('$\sqrt{(x_{k+1}-x_{k})^2}$','interpreter','latex')
%% 4)
sparse_sig = my_sparsesig(100, 5, 2);
h = [1 2 3 4 3 2 1]/16;
y = conv(sparse_sig, h);
yn = y + 0.05 * randn(length(y), 1);
lambda = 0.1;
alpha = 1;
Nit = 2000;
H = convmtx(h', 100);
[deconv_sig, j] = ista(yn, H, lambda, alpha, Nit);
figure
subplot(5, 1, 1)
stem(sparse_sig)
title('input')
subplot(5, 1, 2)
stem(h)
title('impose response')
subplot(5, 1, 3)
stem(yn)
title('noise output')
subplot(5, 1, 4)
stem(deconv_sig)
ylim([0 2])
title('ESTIMATED SIGNAL')
subplot(5, 1, 5)
plot(j)
title('OBJECTIVE FUNCTION')