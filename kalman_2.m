clear
clc
[y, Fs] = audioread('sp1.wav');
y = y(1: 2*Fs);
figure
subplot(3, 1, 1)
plot(y)
xlabel('time')
ylabel('magnitude')
title('original signal')
y = y + 0.05 * randn(length(y), 1);
%sound(y,Fs)
subplot(3, 1, 2)
plot(y)
xlabel('time')
ylabel('magnitude')
title('distortion signal')
N = 64;                      % length of the frame
m = N/2;                      % 50% overlap for each frame
length = length(y);           % input signal length
count = floor(length/m)-1;    % move frame how many times, ignore the zeropadding
p = 5;                       % the order of AR module
a = zeros(1, p);
window = hamming(N);          % use hamming window
F = zeros(p, p);              % state transition matrix
for i = 1:p-1
    F(i, i+1) = 1;
end
H = [zeros(1, p-1) 1];        % transform matrix  maps the state vector parameters into the measurement domain
S0 = zeros(p, 1);             % initial state
P = zeros(p);

y_temp = cov(y(1:(count+1)*m));
T = zeros(length, 1);
ss = zeros(N, count);
s_out = zeros((count+1)*m, 1);

for r = 1:count
    y_frame = y((r-1)*m+1:(r+1)*m);
    if r == 1
        [a, VS] = lpc(y_frame, p);
    else
        [a, VS] = lpc(T((r-2)*m+1:(r-2)*m+N),p);
    end
    
    if VS - 0.05 > 0
        VS = VS - y_temp;
    else
        VS = 0.0005;
    end
    
    F(p, :) = -1*a(p+1:-1:2);
    
    for j = 1:N
        if j == 1
            S_ = F * S0;
            P_ = F*P*F' + VS;
        else
            S_ = F * S;
            P_ = F*P*F' + VS;
        end
        K = P_*H'/(y_temp+H*P_*H');
        P = (eye(p) - K*H)*P_;
        S = S_ + K*(y_frame(j)-H*S_);
        T((r-1)*m+j) = H*S;
    end
    ss(:, r) = T((r-1)*m+1:(r-1)*m+N).*window;
end

for r = 1:count-1
    if r == 1
        s_out(1:N) = ss(1:N, r);
    else
        s_out(r*m+1:r*m+N) = s_out(r*m+1:r*m+N) + ss(1:N, r);
    end
end
subplot(3, 1, 3)
plot(s_out)
xlabel('time')
ylabel('magnitude')
title('Kalman Filtered signal')