function hist_eq(matrix)
matrix = int8(matrix);
[M, N] = size(matrix);
x = zeros(256,1);
for i = 1:M
    for j = 1:N;
        f = matrix(i,j);
        x(f+1) = x(f+1)+1;
    end
end
x = x/(M*N);
C = zeros(256,1);
for k = 1:128
    C(k) = uint8(sum(x(1:k))*255);
end
histeqimg = zeros(M,N);
for i = 1:M
    for j = 1:N
        f = int8(matrix(i,j))+1;
        histeqimg(i,j) = C(f);
    end
end
figure;
subplot(1,2,1)
imshow(matrix);
subplot(1,2,2)
imshow(int8(histeqimg));