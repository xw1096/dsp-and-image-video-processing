%% load image
original_img1 = imread('/home/code/Documents/Samples/barbara_gray.bmp');
original_img2 = imread('/home/code/Documents/Samples/lena_gray.bmp');
%% upsample and downsample
figure
subplot(2,2,1)
img1 = mydownsample2(original_img1, 'None');
img1 = upsamplefilter(img1, 'Bilinear');
imshow(img1)
title('downsample None, upsample Bilinear')
subplot(2,2,2)
img2 = mydownsample2(original_img1, 'None');
img2 = upsamplefilter(img2, 'Bicubic');
imshow(img2)
title('downsample None, upsample Bicubic')
subplot(2,2,3)
img3 = mydownsample2(original_img1, 'Averaging');
img3 = upsamplefilter(img3, 'Bilinear');
imshow(img3)
title('downsample Averaging, upsample Bilinear')
subplot(2,2,4)
img4 = mydownsample2(original_img1, 'Averaging');
img4 = upsamplefilter(img4, 'Bicubic');
imshow(img4)
title('downsample Averaging, upsample Bicubic')
%% affine transform
[m, n] = size(original_img2);
r = 700;
c = 700;
a = [-0.710847606499752,0.865612648221344,0.501010101010101];
b = [2.552891523935002e+02,-0.498023715415020,0.864646464646465];
new = zeros(r, c);
for i = 1:m
    for j = 1:n
        new(round([1 i j]*a'),round([1 i j]*b')) = original_img2(i, j);
    end
end
for i = 2:r-1
    for j = 1:c
        if new(i, j) == 0
            new(i, j) = new(i-1,j)/2 + new(i+1,j)/2;
        end
    end
end
new = uint8(new);
figure
subplot(2,1,1)
imshow(original_img2)
subplot(2,1,2)
imshow(new)